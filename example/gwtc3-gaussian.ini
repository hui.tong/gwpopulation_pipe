################################################################################
## Generic arguments
################################################################################

run-dir=GWTC-3-gaussian
log-dir=GWTC-3-gaussian/logs
label=gaussian
# This option is only relevant if you are running on a cluster that requires
# a user accounting tag.
user=colm.talbot
# use the vt-file from the docker image used for testing that already has the
# FAR and SNR cuts applied
vt-file=/test-data/sensitivity/o1+o2+o3_bbhpop_real+semianalytic-LIGO-T2100377-v2-far-1-snr-10.hdf5
vt-ifar-threshold=1.0
vt-snr-threshold=10.0
vt-function=injection_resampling_vt
prior-file=gaussians.prior
# the test image does not have a GPU, so we set this to 0
request-gpu=0
# the backend must be jax to use the NUTS sampler
backend=jax

################################################################################
## Analysis models
# A tensor product will be done with the models listed here, i.e., if you list
# two mass models, two spin magnitude models, one spin tilt model, and two
# redshift models, a total of 8 analysis jobs will be set up
################################################################################

all-models={all: [gaussian_models.all_the_gaussians]}

################################################################################
## Data collection arguments
################################################################################

parameters=[mass_1, mass_2, a_1, a_2, cos_tilt_1, cos_tilt_2, redshift]
ignore=[GW170817, S190425z, GW200115, 190403, 190426, 190514, S190814bv, 190916, GW190917, 190926, 191113, 191126, GW191219, 200105, 200115, 200210, 200220, 200306, 200308, 200322]
# the sample files point to the location in the docker image used for testing
# we downsample the events by month to make the tests faster but still cover
# each observing run
sample-regex={"GWTC1": "/test-data/gwtc1-downsampled/*GW1708*.h5", "O3a": "/test-data/o3a-downsampled/*GW1908*.h5", "O3b": "/test-data/o3b-downsampled/*GW1911*.h5"}
preferred-labels=[PrecessingSpinIMRHM, C01:IMRPhenomXPHM, IMRPhenomXPHM]
plot=True
data-label=posteriors
distance-prior=comoving
mass-prior=flat-detector-components
spin-prior=component
samples-per-posterior=500

################################################################################
## Arguments describing analysis jobs
################################################################################

max-redshift=1.9
minimum-mass=2
maximum-mass=100
# sampler settings chosen to make the tests faster
# these are likely not good settings for a real analysis
sampler=numpyro
sampler-kwargs={sampler_name: "NUTS", num_warmup: 50, num_samples: 100}
vt-parameters=[all]
enforce-minimum-neffective-per-event=False

################################################################################
## Post processing arguments
################################################################################

post-plots=True
make-summary=False
n-post-samples=5000
